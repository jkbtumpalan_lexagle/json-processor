# EXER-006: Create a Kotlin program that processes a JSON file using Jackson

## Objectives:

- Learn how to create a base Kotlin program and how to run it
- Learn how to create classes in Kotlin
- Learn how to interact with files in the file system
- Learn how to manipulate and process JSON in Kotlin

## Requirements:

- [ ]  Create a Kotlin program that can do the following
    - [ ]  combines all the json files into one json file using Jackson library
    - [ ]  separate the combined file into multiple json files (grouped by country)
    - [ ]  output file name is <country>.json

## Bonus:

- [ ]  Generate a runnable `.jar` file and run the program via command line
- [ ]  Create a program that is CLI-friendly wherein I can specify program arguments for the ff.
    - [ ]  input directory of json files
    - [ ]  output directory
- [ ]  Bloat the test files to around 5GB and use it for your program
